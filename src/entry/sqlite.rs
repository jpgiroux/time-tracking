//! Sqlite module for the Simple Time Tracking crate.
//! This module is used for persistence of the entries.

use super::Entry;
use rusqlite::{Connection, Result, params};
use chrono::NaiveDate;
use std::error::Error;

/// Create the table "entries" if it does not exists, otherwise does nothing
pub fn create_schema(db: &str) -> Result<()> {
    let conn = Connection::open(db)?;

    conn.execute(
        "CREATE TABLE IF NOT EXISTS entries (
            day date NOT NULL,
            start_time time NOT NULL,
            stop_time time,
            description TEXT,
            PRIMARY KEY(day, start_time)
        )",
        []
    )?;

    Ok(())
}

/// Get the entries between 2 dates
pub fn get_entries_between(db: &str, l: &NaiveDate, r: &NaiveDate) -> Result<Vec<Entry>, Box<dyn Error>> {
    let conn = Connection::open(db)?;

    let mut stmt = conn.prepare("SELECT * FROM entries
        WHERE day BETWEEN ?1 AND ?2
        ORDER BY day, start_time, stop_time")?;
    let entry_iter = stmt.query_map(
        [
            l,
            r,
        ], |row| {
            Ok(
                Entry {
                    day: row.get(0)?,
                    start_time: row.get(1)?,
                    stop_time: row.get(2)?,
                    description: row.get(3)?,
                }
            )
        })?;

    Ok(entry_iter.map(|e| e.unwrap()).collect())
}

/// Save an entry, if it has a stop time, it will be updated, if it does not, it will be inserted.
pub fn save_entry(db: &str,e: &Entry) -> Result<(), Box<dyn Error>> {
    let conn = Connection::open(db)?;

    match e.stop_time {
        Some(st) => {
            conn.execute(
                "UPDATE entries
             SET stop_time = ?1, description = ?2
             WHERE day = ?3 AND start_time = ?4",
                params![st, e.description.as_ref().unwrap(), e.day, e.start_time])?;
        },
        None => {
            conn.execute(
                "INSERT INTO entries (day, start_time)
             VALUES(?1, ?2)",
                params![e.day, e.start_time])?;
        },
    };
    Ok(())
}

