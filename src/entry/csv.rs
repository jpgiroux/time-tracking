//! csv module for the Simple Time Tracking crate.
//! This module is used for exportation of the current week in a csv file.

use super::Entries;
use std::error::Error;

/// Write the entries into a file with a header.
pub fn write_to_file(filename: &str, entries: &Entries) -> Result<(), Box<dyn Error>> {
    let mut wtr = csv::Writer::from_path(filename)?;

    wtr.write_record(&["day", "start_time", "stop_time", "description"])?;

    for entry in &entries.entries {
        wtr.write_record(entry.to_record())?;
    }

    wtr.flush()?;

    Ok(())
}

