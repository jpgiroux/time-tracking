use clap::{App, Arg, SubCommand};
use chrono::{Duration, NaiveDate, Weekday, Local, Datelike, ParseError};
use std::str::FromStr;


pub fn to_hhmm(duration: &Duration) -> String {
	format!("{:02}:{:02}", duration.num_hours(), duration.num_minutes() - (duration.num_hours() * 60))
}

fn main() {
	let op = handle_args();

	if op.is_err() {
		eprintln!("{}", op.err().unwrap())
	} else {
		let op = op.ok().unwrap();

		let mut config_file = dirs::config_dir().unwrap();
		config_file.push("time_tracking/Settings");

		let config = config::Config::builder()
			.add_source(config::File::with_name(config_file.to_str().unwrap()))
			.build()
			.unwrap()
			.try_deserialize::<time_tracking::Config>()
			.unwrap();

		let result = time_tracking::run(op, config);

		match result {
			Ok(runinfo) => {
				println!("Current day {}", runinfo.current_day);
				println!("Time that will be use {}", runinfo.time_used);
				println!("Week period {} - {}", runinfo.start_week, runinfo.end_week);
				println!("Time worked today: {}", to_hhmm(&runinfo.time_worked_today));
				println!("Time worked this week: {}", to_hhmm(&runinfo.time_worked));
				println!("Time remaining for a {} work week: {}", to_hhmm(&runinfo.work_week), to_hhmm(&runinfo.time_remaining));
			},
			Err(e) => { eprintln!("Error during run: {}", e) }
		}
	}


}

fn handle_args() -> Result<Option<time_tracking::Operation>, ParseError> {

	let now = Local::now().naive_local();
	let start_week = NaiveDate::from_isoywd(now.year(), now.iso_week().week(), Weekday::Mon);
	let end_week = NaiveDate::from_isoywd(now.year(), now.iso_week().week(), Weekday::Sun);

	let matches = App::new("Simple Time Tracking")
			.version("1.0")
			.author("jpgiroux <girouxjep@gmail.com>")
			.about("Simple time tracker for work")
			.subcommand(SubCommand::with_name("start")
				.about("Start a new entry at current time")
			)
			.subcommand( SubCommand::with_name("stop")
				.about("Stop the last entry at current time")
				.arg(Arg::with_name("description")
					.required(true)
					.takes_value(true)
				)
			)
			.subcommand(SubCommand::with_name("export")
				.about("Export the current week entries into a csv file")
				.arg(Arg::with_name("filename")
					.required(true)
					.takes_value(true)
				)
				.arg(Arg::with_name("from")
					.required(false)
					.takes_value(true)
				)
				.arg(Arg::with_name("to")
					.required(false)
					.takes_value(true)
				)
			)
			.get_matches();

	if matches.subcommand_matches("start").is_some() {
		Ok(Some(time_tracking::Operation::Start))
	} else if matches.subcommand_matches("stop").is_some() {
		let m = matches.subcommand_matches("stop").unwrap();
		Ok(Some(time_tracking::Operation::Stop(m.value_of("description").unwrap().to_string())))
	} else if matches.subcommand_matches("export").is_some() {
		let m = matches.subcommand_matches("export").unwrap();
		Ok(Some(time_tracking::Operation::Export(m.value_of("filename").unwrap().to_string(),
		  m.value_of("from").map(|t| Ok(NaiveDate::from_str(t)?)).unwrap_or(Ok(start_week))?,
		  m.value_of("to").map(|t| Ok(NaiveDate::from_str(t)?)).unwrap_or(Ok(end_week))?
		)))
	} else {
		Ok(None)
	}
}




