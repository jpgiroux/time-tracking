//! Entry module that contain the Entry and Entries struct

pub(crate) mod sqlite;
pub(crate) mod csv;

use chrono::{NaiveDate, NaiveTime, Duration};
use std::fmt;

pub struct Entry {
    day: NaiveDate,
    start_time: NaiveTime,
    pub (crate) stop_time: Option<NaiveTime>,
    description: Option<String>,
}

impl Entry {

    pub fn start(day: NaiveDate, start_time: NaiveTime) -> Entry {
        Entry {
            day,
            start_time,
            stop_time: None,
            description: None,
        }
    }

    pub fn is_same_day(&self, date: NaiveDate) -> bool {
        self.day == date
    }

    pub fn duration(&self) -> Duration {
        match self.stop_time {
            Some(st) => st - self.start_time,
            None => Duration::seconds(0),
        }
    }

    pub fn duration_from_start(&self, st: NaiveTime) -> Duration {
        st - self.start_time
    }

    pub fn duration_from_stop(&self, st: NaiveTime) -> Option<Duration> {
        match self.stop_time {
            Some(stop_time) => Some(st - stop_time),
            None => None
        }
    }

    pub fn in_progress(&self) -> bool {
        self.stop_time == None
    }

    pub fn stop(&mut self, st: NaiveTime, desc: String) {
        if self.stop_time == None {
            self.stop_time = Some(st);
            self.description = Some(desc);
        }
    }

    pub fn to_record(&self) ->  [String; 4] {
        [
            self.day.to_string(),
            self.start_time.to_string(),
            self.stop_time.map_or(String::from(""), |st| st.to_string()),
            self.description.as_ref().unwrap_or(&String::from("")).to_string(),
        ]
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} from {} to {} doing: {}",
               self.day,
               self.start_time,
               self.stop_time.map_or("--".to_string(), |st| format!("{}", st)),
               self.description.as_ref().unwrap_or(&"--".to_string()))
    }
}

pub struct Entries {
    pub (crate) entries: Vec<Entry>
}

impl Entries {

    pub fn get_last(&self) -> Option<&Entry> {
        self.entries.last()
    }

    pub fn get_last_mut(&mut self) -> Option<&mut Entry> {
        self.entries.last_mut()
    }

    pub fn add(&mut self, entry: Entry) {
        self.entries.push(entry);
    }

    pub fn get_duration_worked_until_now(&self, now: NaiveTime) -> Duration {
        Entries::get_duration_worked_until_now_from(self.entries.iter().collect(), now)
    }

    pub fn get_duration_worked_for_today_until_now(&self, today: NaiveDate, now: NaiveTime) -> Duration {
        Entries::get_duration_worked_until_now_from(self.entries.iter().filter(|e| e.is_same_day(today)).collect(), now)
    }

    fn get_duration_worked_until_now_from(entries : Vec<&Entry>, now: NaiveTime) -> Duration {
        entries.iter()
            .fold(
                Duration::minutes(0),
                |acc, entry|
                    acc + entry.duration()
            ) + entries.last().map_or(Duration::minutes(0), |last| if last.in_progress() { last.duration_from_start(now) } else { Duration::minutes(0) })
    }
}

#[cfg(test)]
mod tests {

    use super::Entry;
    use chrono::{Local, Duration};
    use std::ops::Sub;

    #[test]
    fn when_not_stopped_entry_then_is_in_progress() {
        let now = Local::now().naive_local();
        let e = Entry::start(now.date(), now.time());
        assert!(e.in_progress());
    }

    #[test]
    fn when_stopped_entry_then_not_in_progress() {
        let now = Local::now().naive_local();
        let mut e = Entry::start(now.date(), now.time());
        e.stop(now.time().sub(Duration::minutes(6)), "testing!!".to_string());
        assert!(!e.in_progress());
    }

    #[test]
    fn when_not_stopped_entry_then_duration_from_stopped_is_none() {
        let now = Local::now().naive_local();
        let e = Entry::start(now.date(), now.time());
        assert_eq!(e.duration_from_stop(now.time()), None);
    }

    #[test]
    fn when_stopped_entry_6_minutes_ago_then_duration_from_stop_is_6_minutes() {
        let now = Local::now().naive_local();
        let mut e = Entry::start(now.date(), now.time());
        e.stop(now.time().sub(Duration::minutes(6)), "testing!!".to_string());
        assert_eq!(e.duration_from_stop(now.time()), Some(Duration::minutes(6)));
    }
}