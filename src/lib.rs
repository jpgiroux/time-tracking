//! A Simple Time Tracking library
//!
//! # Examples
//! ```
//! let op = time_tracking::Operation::Start;
//! let conf = time_tracking::Config{
//!     minutes_per_day: 8 * 60,
//!     day_per_week: 5,
//!     minutes_buffer: 5,
//!     sqlite_db: "./stt.db".to_string()
//! };
//!
//! let runInfo = time_tracking::run(Some(op), conf);
//! ```

pub mod entry;

use crate::entry::{Entry, Entries, sqlite, csv};
use chrono::{NaiveDate, Duration, Local, DurationRound, Datelike, Weekday, NaiveTime, NaiveDateTime};
use std::error::Error;
use serde::Deserialize;

/// Available operation that you can do.
#[derive(Debug)]
pub enum Operation {
    ///Start a new entry on current day and a rounded time.
    Start,
    ///Stop the last entry with current rounded time and a given description.
    Stop(String),
    ///Export entries between dates in the file given.
    Export(String, NaiveDate, NaiveDate),
}

/// Configuration of the library.
#[derive(Debug, Deserialize)]
pub struct Config{
    pub minutes_per_day: u32,
    ///This and minutes_per_day are used to calculate the time goal per week.
    pub day_per_week : u32,
    ///A minute buffer used for when you do a stop followed with a new start (i.e task switch)
    /// If the start time is within the buffer with the stop_time of the last entry, the start_time use
    /// will be the same as the stop_time.
    pub minutes_buffer : u32,
    ///Path to the sqlite database.
    pub sqlite_db : String,
}

/// The output of the run.
pub struct RunInfo{
    pub current_day: NaiveDate,
    pub time_used: NaiveTime,
    pub start_week: NaiveDate,
    pub end_week: NaiveDate,
    pub work_week: Duration,
    pub time_worked: Duration,
    pub time_worked_today: Duration,
    pub time_remaining: Duration,
}

/// The main entry point of the lib
pub fn run(op: Option<Operation>, config: Config) -> Result<RunInfo, Box<dyn Error>> {
    sqlite::create_schema(&config.sqlite_db.as_str())?;

    let duration_buffer = Duration::minutes(config.minutes_buffer as i64);

    let now = Local::now().naive_local();
    let now_rounded = Local::now().duration_round(duration_buffer).unwrap().naive_local();

    let current_day = now.date();
    let start_week = NaiveDate::from_isoywd(now.year(), now.iso_week().week(), Weekday::Mon);
    let end_week = NaiveDate::from_isoywd(now.year(), now.iso_week().week(), Weekday::Sun);

    let mut entries = get_entries_between(&config, &start_week, &end_week)?;

    let time_used = get_time_to_use(entries.get_last(), duration_buffer, now, now_rounded.time());

    let work_week = Duration::minutes((config.minutes_per_day  * config.day_per_week) as i64);

    match op {
        Some(op) => match op {
            Operation::Start => {
                //should validate any unstop entries
                if entries.get_last().is_some() && entries.get_last().unwrap().in_progress() {
                    Err("Last entry still in progress, stop it before starting a new one")?;
                }
                let new_entry = Entry::start(current_day, time_used);

                save_entry(&config, &new_entry)?;
                entries.add(new_entry);
            },
            Operation::Stop(desc) => {

                if entries.get_last().is_none() {
                    Err("No previous entry to stop")?;
                }

                if !entries.get_last().unwrap().in_progress() {
                    Err("The last entry is already stopped.")?;
                }

                let last = entries.get_last_mut().unwrap();
                last.stop(time_used, desc);

                save_entry(&config, &last)?;
            },
            Operation::Export(filename, from, to ) => {
                write_to_file(&filename, &get_entries_between(&config, &from, &to)?)?;
            }
        },
        None => {}
    };
    let  time_worked= entries.get_duration_worked_until_now(time_used);
    let time_worked_today =  entries.get_duration_worked_for_today_until_now(now.date(), time_used);
    let time_remaining = work_week - time_worked;
    Ok(
        RunInfo {
            current_day,
            time_used,
            start_week,
            end_week,
            work_week,
            time_worked,
            time_worked_today,
            time_remaining
        }
    )
}

/// Find the time to use base on current time, last entry and the duration treshold
fn get_time_to_use(last_entry: Option<&Entry>, duration_threshold: Duration, now: NaiveDateTime, now_rounded: NaiveTime) -> NaiveTime {
    match last_entry {
        Some(last) => if last.is_same_day(now.date()) {
            match last.duration_from_stop(now.time()) {
                Some (duration) => if duration <= duration_threshold { last.stop_time.unwrap() } else { now_rounded },
                None => now_rounded
            }
        }else { now_rounded } ,
        None => now_rounded
    }
}

/// Get the entries between 2 dates
fn get_entries_between(config: &Config,l: &NaiveDate, r: &NaiveDate) -> Result<Entries, Box<dyn Error>> {
    let entries = sqlite::get_entries_between(&config.sqlite_db.as_str(), l, r)?;
    Ok( Entries { entries } )
}

/// Save the entry
fn save_entry(config: &Config, entry: &Entry) -> Result<(), Box<dyn Error>> {
    sqlite::save_entry(&config.sqlite_db.as_str(), entry)
}

/// Write the entries into file
fn write_to_file(filename: &str, entries: &Entries) -> Result<(), Box<dyn Error>> {
    csv::write_to_file(filename, entries)
}